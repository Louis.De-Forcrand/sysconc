class NoVisibility {
	private static boolean ready; // = false;
	public static int number; // = 0;
	public static class ReaderThread extends Thread {
		public void run() {
			while(!ready)
				Thread.yield();
			System.out.println(number);
		}
	}

	public static void main(String[] args) {
		number = 24;
		new ReaderThread().start();
		number = 42;
		ready = true;
	}
}
