class volatileTest {
	private static int buf;
	public static void main(String args[]) {
		buf = 0;
		new WriterThread(5).start();
		new WriterThread(8).start();
		new ReaderThread().start();
	}
	private int read () {
		return buf;
	}
	private void write(int val) {
		int temp = buf;
		buf = val;
	}
	private static class ReaderThread extends Thread {
		public void run() {
			System.out.println(buf);
		}
	}
	private static class WriterThread extends Thread {
		private int val;
		public WriterThread(int init) {
			val = init;
		}
		public void run() {
			buf = val;
		}
	}
}
